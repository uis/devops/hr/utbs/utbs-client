/* === AUTO-GENERATED - DO NOT EDIT === */

/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.utbs.methods;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import uk.ac.cam.ucs.utbs.client.ClientConnection;
import uk.ac.cam.ucs.utbs.client.ClientConnection.Method;
import uk.ac.cam.ucs.utbs.client.UTBSException;
import uk.ac.cam.ucs.utbs.dto.*;

/**
 * Common methods for searching for objects in the UTBS database.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class UTBSMethods
{
    // The connection to the server
    private ClientConnection conn;

    /**
     * Create a new UTBSMethods object.
     *
     * @param conn The ClientConnection object to use to invoke methods
     * on the server.
     */
    public UTBSMethods(ClientConnection conn)
    {
        this.conn = conn;
    }

    /**
     * Get the currently authenticated API user.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/current-user ]</code>
     *
     * @return The API user, or null if authentication details have not been
     * supplied.
     */
    public String getCurrentUser()
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = {  };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/current-user",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.value;
    }

    /**
     * Get full list of privileges that the currently authenticated user has,
     * including role privileges.
     * <p>
     * If a provider is not specified, this will only include privileges
     * granted to all providers. Otherwise it will include privileges granted
     * for the specified provider, in addition to all-provider privileges.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/privileges ]</code>
     *
     * @param provider [optional] The short name of a provider.
     * @return The user's privileges (separated by newlines).
     */
    public String getPrivileges(String  provider)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = { "provider", provider };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/privileges",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.value;
    }

    /**
     * Get the current API version number.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/version ]</code>
     *
     * @return The API version number string.
     */
    public String getVersion()
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = {  };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/version",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.value;
    }
}
