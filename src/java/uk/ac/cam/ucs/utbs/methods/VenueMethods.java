/* === AUTO-GENERATED - DO NOT EDIT === */

/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.utbs.methods;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import uk.ac.cam.ucs.utbs.client.ClientConnection;
import uk.ac.cam.ucs.utbs.client.ClientConnection.Method;
import uk.ac.cam.ucs.utbs.client.UTBSException;
import uk.ac.cam.ucs.utbs.dto.*;

/**
 * Methods for querying and manipulating venues.
 *
 * <h4>The fetch parameter for venues</h4>
 * <p>
 * All methods that return venues also accept an optional <code>fetch</code>
 * parameter that may be used to request additional information about the
 * venues returned. For more details about the general rules that apply to
 * the <code>fetch</code> parameter, refer to the {@link EventMethods}
 * documentation.
 * <p>
 * For venues the <code>fetch</code> parameter may be used to fetch the
 * venue's provider:
 * <ul>
 * <li>{@code "provider"} - fetches the venue's provider.</li>
 * </ul>
 * <p>
 * As with the event <code>fetch</code> parameter, this reference may be used
 * in a chain by using the "dot" notation to fetch additional information
 * about referenced provider. For example "provider.venues" will fetch the
 * venue's provider and all the other venues belonging to that provider. For
 * more information about what can be fetched from the referenced provider,
 * refer to the documentation for {@link ProviderMethods}.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class VenueMethods
{
    // The connection to the server
    private ClientConnection conn;

    /**
     * Create a new VenueMethods object.
     *
     * @param conn The ClientConnection object to use to invoke methods
     * on the server.
     */
    public VenueMethods(ClientConnection conn)
    {
        this.conn = conn;
    }

    /**
     * Return a list of all venues.
     * <p>
     * By default, only a few basic details about each venue are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/venue/all-venues ]</code>
     *
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch for each venue.
     *
     * @return The requested venues, in name order.
     */
    public java.util.List<UTBSVenue> getAllVenues(String    fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/venue/all-venues",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.venues;
    }

    /**
     * Get the venue with the specified ID.
     * <p>
     * By default, only a few basic details about the venue are returned, but
     * the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/venue/{id} ]</code>
     *
     * @param id [required] The ID of the venue to fetch.
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch.
     *
     * @return The requested venue or null if it was not found.
     */
    public UTBSVenue getVenue(Long      id,
                              String    fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = { ""+id };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/venue/%1$s",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.venue;
    }

    /**
     * Get the events held in a venue in the specified time period.
     * <p>
     * This will return any events that overlap the specified time period.
     * More specifically, it will return events whose start is less than or
     * equal to the end of the time period, and whose end is greater than or
     * equal to the start of the time period (i.e., all the start and end
     * timestamps are treated inclusively).
     * <p>
     * Optionally, this will also check the event's sessions and exclude any
     * events that have no sessions overlapping the specified time period.
     * This can happen for events with multiple sessions. For example,
     * suppose an event has 2 sessions, one on Monday and the other on
     * Friday, and that the specified time period to search was on Wednesday.
     * Then by default, the event would be returned, because it starts on
     * Monday and ends on Friday, which overlaps the time period being
     * searched, but if session checking is enabled, the event would be
     * excluded.
     * <p>
     * By default, only a few basic details about each event are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references.
     * <p>
     * NOTE: When using this API directly via the URL endpoints, date-time
     * parameters should be supplied as either milliseconds since epoch, or
     * as ISO 8601 formatted date or date-time strings.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/venue/{id}/events-in-time-period ]</code>
     *
     * @param id [required] The ID of the venue.
     * @param start [optional] The start of the time period to search. If
     * omitted, this will default to 0:00am today.
     * @param end [optional] The end of the time period to search. If
     * omitted, this will default to the first midnight after the start date.
     * @param checkSessions [optional] If {@code true}, check the event
     * sessions, and exclude any events that have no sessions overlapping the
     * the time period being searched.
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch for each event.
     *
     * @return A list of events found, in (start date-time, ID) order.
     */
    public java.util.List<UTBSEvent> getEventsInTimePeriod(Long             id,
                                                           java.util.Date   start,
                                                           java.util.Date   end,
                                                           boolean          checkSessions,
                                                           String           fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = { ""+id };
        Object[] queryParams = { "start", start,
                                 "end", end,
                                 "checkSessions", checkSessions,
                                 "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/venue/%1$s/events-in-time-period",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.events;
    }
}
