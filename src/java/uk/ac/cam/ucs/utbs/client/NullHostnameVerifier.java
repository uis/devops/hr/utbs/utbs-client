package uk.ac.cam.ucs.utbs.client;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

/**
 * Null hostname verifier for checking hostnames in certificates. This
 * disables all checking, and just trusts the server.
 * <p>
 * This should only be used for testing, when connecting to a server that
 * doesn't have valid certificates.
 */
/* package */ class NullHostnameVerifier implements HostnameVerifier
{
    @Override
    public boolean verify(String hostname, SSLSession session)
    {
        return true; // Allow all hosts
    }
}
