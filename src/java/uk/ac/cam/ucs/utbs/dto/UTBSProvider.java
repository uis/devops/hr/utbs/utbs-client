/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.utbs.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import uk.ac.cam.ucs.utbs.dto.UTBSResult.EntityMap;

/**
 * Class representing a training provider returned by the web service API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class UTBSProvider
{
    /** The provider's unique short name. */
    @XmlElement public String shortName;

    /**
     * The provider's full name (as it appears in the provider selection
     * menu of the UTBS).
     */
    @XmlElement public String name;

    /** The provider's title (as it appears in the title bar of the UTBS). */
    @XmlElement public String title;

    /**
     * The institution that owns the provider. Only the
     * {@link UTBSInstitution#instid instid} field will be populated unless
     * the <code>fetch</code> parameter included the "institution" option.
     */
    @XmlElement public UTBSInstitution institution;

    /** The unique URL prefix used for the provider's pages in the UTBS. */
    @XmlElement public String urlPrefix;

    /** The provider's contact email address. */
    @XmlElement public String contactEmail;

    /** The provider's contact phone number. */
    @XmlElement public String phoneNumber;

    /** The Wiki text for the provider's main home page. */
    @XmlElement public String homePage;

    /** The Wiki text describing the venues belonging to the provider. */
    @XmlElement public String venueInfo;

    /**
     * The provider's current programme. This will only be populated if the
     * <code>fetch</code> parameter included the "current_programme" option.
     */
    @XmlElement public UTBSProgramme currentProgramme;

    /**
     * A list of all the provider's programmes. This will only be populated
     * if the <code>fetch</code> parameter included the "programmes"
     * option.
     */
    @XmlElementWrapper
    @XmlElement(name = "programme")
    public List<UTBSProgramme> programmes;

    /**
     * A list of all the themes belonging to the provider. This will only be
     * populated if the <code>fetch</code> parameter included the "themes"
     * option.
     */
    @XmlElementWrapper
    @XmlElement(name = "theme")
    public List<UTBSTheme> themes;

    /**
     * A list of all the venues belonging to the provider. This will only be
     * populated if the <code>fetch</code> parameter included the "venues"
     * option.
     */
    @XmlElementWrapper
    @XmlElement(name = "venue")
    public List<UTBSVenue> venues;

    /* == Attributes/methods for the flattened XML/JSON representation == */

    /**
     * An ID that can uniquely identify this provider within the returned
     * XML/JSON document. This is only used in the flattened XML/JSON
     * representation (if the "flatten" parameter is specified).
     */
    @XmlAttribute public String id;

    /**
     * A reference (by id) to a provider element in the XML/JSON document.
     * This is only used in the flattened XML/JSON representation (if the
     * "flatten" parameter is specified).
     */
    @XmlAttribute public String ref;

    /** Compute an ID that can uniquely identify this entity in XML/JSON. */
    public String id() { return "provider/"+shortName; }

    /** Return a new UTBSProvider that refers to this provider. */
    public UTBSProvider ref() { UTBSProvider p = new UTBSProvider(); p.ref = id; return p; }

    /* == Code to help clients unflatten a UTBSProvider object == */

    /* Flag to prevent infinite recursion due to circular references. */
    private boolean unflattened;

    /** Unflatten a single UTBSProvider. */
    protected UTBSProvider unflatten(EntityMap  em)
    {
        if (ref != null)
        {
            UTBSProvider provider = em.getProvider(ref);
            if (!provider.unflattened)
            {
                provider.unflattened = true;
                if (provider.institution != null)
                    provider.institution = provider.institution.unflatten(em);
                if (provider.currentProgramme != null)
                    provider.currentProgramme = provider.currentProgramme.unflatten(em);
                UTBSProgramme.unflatten(em, provider.programmes);
                UTBSTheme.unflatten(em, provider.themes);
                UTBSVenue.unflatten(em, provider.venues);
            }
            return provider;
        }
        return this;
    }

    /** Unflatten a list of UTBSProvider objects (done in place). */
    protected static void unflatten(EntityMap           em,
                                    List<UTBSProvider>  providers)
    {
        if (providers != null)
            for (int i=0; i<providers.size(); i++)
                providers.set(i, providers.get(i).unflatten(em));
    }
}
