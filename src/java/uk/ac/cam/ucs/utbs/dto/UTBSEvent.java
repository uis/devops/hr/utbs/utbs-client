/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.utbs.dto;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import uk.ac.cam.ucs.utbs.dto.UTBSResult.EntityMap;

/**
 * Class representing an event returned by the web service API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class UTBSEvent
{
    /** The unique identifier of the event. */
    @XmlAttribute public Long eventId;

    /**
     * The event's provider. This will only be populated if the
     * <code>fetch</code> parameter included the "provider" option.
     */
    @XmlElement public UTBSProvider provider;

    /**
     * The programme to which the event belongs. This will only be populated
     * if the <code>fetch</code> parameter included the "programme" option.
     */
    @XmlElement public UTBSProgramme programme;

    /** The course that the event is for. */
    @XmlElement public UTBSCourse course;

    /** The start date and time of the event. */
    @XmlElement public Date startDate;

    /** The end date and time of the event. */
    @XmlElement public Date endDate;

    /** The event's title. */
    @XmlElement public String title;

    /**
     * The event's course type. This will be one of "instructor-led",
     * "self-taught" or "facility-booking".
     */
    @XmlElement public String courseType;

    /**
     * The sub-type of the event. The possible values vary, depending on the
     * event's course type.
     */
    @XmlElement public String subType;

    /**
     * The event's status ("published", "completed", "postponed" or
     * "cancelled").
     */
    @XmlElement public String status;

    /** The event's target audience. */
    @XmlElement public String targetAudience;

    /** The event's description. */
    @XmlElement public String description;

    /** A textual description of the event's duration. */
    @XmlElement public String duration;

    /** How frequently the event is typically run. */
    @XmlElement public String frequency;

    /** The format of the event (how it is taught). */
    @XmlElement public String format;

    /** Any prerequisites for the event. */
    @XmlElement public String prerequisites;

    /** The aims of the course. */
    @XmlElement public String aims;

    /** The objectives of the course. */
    @XmlElement public String objectives;

    /** For self-taught courses, any system requirements for participants. */
    @XmlElement public String systemRequirements;

    /** Any additional notes about the course. */
    @XmlElement public String notes;

    /** A flag indicating whether this is a new course. */
    @XmlAttribute public Boolean newEvent;

    /** A flag indicating whether this course has been recently updated. */
    @XmlAttribute public Boolean updatedEvent;

    /**
     * A flag indicating whether this is a special event, such as a one-off
     * course that won't be repeated.
     */
    @XmlAttribute public Boolean specialEvent;

    /**
     * A flag indicating whether this is an extra run event added to meet
     * unexpected demand.
     */
    @XmlAttribute public Boolean extraRun;

    /**
     * A flag indicating whether this is an event for beginners, with no
     * significant prerequisites.
     */
    @XmlAttribute public Boolean beginnersEvent;

    /**
     * A flag indicating whether this course assumes significant prerequisite
     * experience from participants.
     */
    @XmlAttribute public Boolean hasPrerequisites;

    /**
     * A flag indicating whether or not this event is shown on the timetable.
     */
    @XmlAttribute public Boolean showOnTimetable;

    /**
     * A flag indicating whether or not this event is shown on the themes
     * page.
     */
    @XmlAttribute public Boolean showOnThemesPage;

    /** The maximum number of participants. */
    @XmlElement public Integer maxParticipants;

    /**
     * A list of the event's topics. This will only be populated if the
     * <code>fetch</code> parameter included the "topics" option.
     */
    @XmlElementWrapper
    @XmlElement(name = "topic")
    public List<UTBSEventTopic> topics;

    /**
     * A list of any extra information sections giving further details about
     * the event. This will only be populated if the <code>fetch</code>
     * parameter included the "extra_info" option.
     */
    @XmlElementWrapper
    @XmlElement(name = "section")
    public List<UTBSEventExtraInfo> extraInfo;

    /**
     * A list of the courses related to the event. This will only be
     * populated if the <code>fetch</code> parameter included the
     * "related_courses" option.
     */
    @XmlElementWrapper
    @XmlElement(name = "course")
    public List<UTBSCourse> relatedCourses;

    /**
     * A list of the event's themes. This will only be populated if the
     * <code>fetch</code> parameter included the "themes" option.
     */
    @XmlElementWrapper
    @XmlElement(name = "theme")
    public List<UTBSTheme> themes;

    /**
     * A list of the event's sessions. This will only be populated if the
     * <code>fetch</code> parameter included the "sessions" option.
     */
    @XmlElementWrapper
    @XmlElement(name = "session")
    public List<UTBSEventSession> sessions;

    /**
     * A list of the venues that the event is held in (typically just one).
     * This will only be populated if the <code>fetch</code> parameter
     * included the "venues" option.
     */
    @XmlElementWrapper
    @XmlElement(name = "venue")
    public List<UTBSVenue> venues;

    /**
     * A list of the event's trainers. This will only be populated if the
     * <code>fetch</code> parameter included the "trainers" option.
     */
    @XmlElementWrapper
    @XmlElement(name = "trainer")
    public List<UTBSSessionTrainer> trainers;

    /**
     * A list of the event's bookings. This will only be populated if the
     * <code>fetch</code> parameter included the "bookings" option, and is
     * only available to authenticated users with appropriate privileges for
     * the event. This may be an incomplete list, if you do not have
     * permission to view all the event's bookings.
     */
    @XmlElementWrapper
    @XmlElement(name = "booking")
    public List<UTBSEventBooking> bookings;

    /* == Attributes/methods for the flattened XML/JSON representation == */

    /**
     * An ID that can uniquely identify this event within the returned
     * XML/JSON document. This is only used in the flattened XML/JSON
     * representation (if the "flatten" parameter is specified).
     */
    @XmlAttribute public String id;

    /**
     * A reference (by id) to an event element in the XML/JSON document.
     * This is only used in the flattened XML/JSON representation (if the
     * "flatten" parameter is specified).
     */
    @XmlAttribute public String ref;

    /** Compute an ID that can uniquely identify this entity in XML/JSON. */
    public String id() { return "event/"+eventId; }

    /** Return a new UTBSEvent that refers to this event. */
    public UTBSEvent ref() { UTBSEvent e = new UTBSEvent(); e.ref = id; return e; }

    /* == Code to help clients unflatten a UTBSEvent object == */

    /* Flag to prevent infinite recursion due to circular references. */
    private boolean unflattened;

    /** Unflatten a single UTBSEvent. */
    protected UTBSEvent unflatten(EntityMap em)
    {
        if (ref != null)
        {
            UTBSEvent event = em.getEvent(ref);
            if (!event.unflattened)
            {
                event.unflattened = true;
                if (event.provider != null)
                    event.provider = event.provider.unflatten(em);
                if (event.programme != null)
                    event.programme = event.programme.unflatten(em);
                if (event.course != null)
                    event.course = event.course.unflatten(em);
                UTBSCourse.unflatten(em, event.relatedCourses);
                UTBSTheme.unflatten(em, event.themes);
                UTBSEventSession.unflatten(em, event.sessions);
                UTBSVenue.unflatten(em, event.venues);
                UTBSEventBooking.unflatten(em, event.bookings);
            }
            return event;
        }
        return this;
    }

    /** Unflatten a list of UTBSEvent objects (done in place). */
    protected static void unflatten(EntityMap       em,
                                    List<UTBSEvent> events)
    {
        if (events != null)
            for (int i=0; i<events.size(); i++)
                events.set(i, events.get(i).unflatten(em));
    }
}
