/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.utbs.dto;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

/**
 * Class representing an individual topic from an event's course description
 * returned by the web service API.
 * <p>
 * The topic may refer to the event as a whole, or be associated with a
 * specific session of the event, if the {@link #sessionNumber} field is set.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class UTBSEventTopic
{
    /**
     * The number of session that the topic is associated with. This may be
     * null for topics describing the event as a whole.
     */
    @XmlAttribute public Integer sessionNumber;

    /** The topic's text. */
    @XmlValue public String topic;
}
