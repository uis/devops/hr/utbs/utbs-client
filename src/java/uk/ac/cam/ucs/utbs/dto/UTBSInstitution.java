/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.utbs.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import uk.ac.cam.ucs.utbs.dto.UTBSResult.EntityMap;

/**
 * Class representing a Lookup institution returned by the web service API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class UTBSInstitution
{
    /** The unique identifier of the institution. */
    @XmlAttribute public String instid;

    /** The institution's short name or acronym. */
    @XmlElement public String shortName;

    /** The institution's full name. */
    @XmlElement public String name;

    /**
     * The institution's parent institution. Only the {@link #instid} field
     * will be populated unless the <code>fetch</code> parameter included the
     * "parent" option.
     */
    @XmlElement public UTBSInstitution parent;

    /**
     * The institution's school institution. Only the {@link #instid} field
     * will be populated unless the <code>fetch</code> parameter included the
     * "school" option.
     */
    @XmlElement public UTBSInstitution school;

    /** The institution's CUFS code. */
    @XmlElement public String cufsCode;

    /**
     * A flag indicating whether this institution is cancelled (and hence no
     * longer exists).
     */
    @XmlAttribute public Boolean cancelled;

    /**
     * A list of the institution's child institutions. This will only include
     * non-cancelled institutions, and will only be populated if the
     * <code>fetch</code> parameter included the "children" option.
     */
    @XmlElementWrapper
    @XmlElement(name = "child")
    public List<UTBSInstitution> children;

    /* == Attributes/methods for the flattened XML/JSON representation == */

    /**
     * An ID that can uniquely identify this institution within the returned
     * XML/JSON document. This is only used in the flattened XML/JSON
     * representation (if the "flatten" parameter is specified).
     */
    @XmlAttribute public String id;

    /**
     * A reference (by id) to an institution element in the XML/JSON
     * document. This is only used in the flattened XML/JSON representation
     * (if the "flatten" parameter is specified).
     */
    @XmlAttribute public String ref;

    /** Compute an ID that can uniquely identify this entity in XML/JSON. */
    public String id() { return "inst/"+instid; }

    /** Return a new UTBSInstitution that refers to this institution. */
    public UTBSInstitution ref()
    {
        UTBSInstitution i = new UTBSInstitution();
        i.ref = id;
        return i;
    }

    /* == Code to help clients unflatten a UTBSInstitution object == */

    /* Flag to prevent infinite recursion due to circular references. */
    private boolean unflattened;

    /** Unflatten a single UTBSInstitution. */
    protected UTBSInstitution unflatten(EntityMap   em)
    {
        if (ref != null)
        {
            UTBSInstitution inst = em.getInstitution(ref);
            if (!inst.unflattened)
            {
                inst.unflattened = true;
                if (inst.parent != null)
                    inst.parent = inst.parent.unflatten(em);
                if (inst.school != null)
                    inst.school = inst.school.unflatten(em);
                UTBSInstitution.unflatten(em, inst.children);
            }
            return inst;
        }
        return this;
    }

    /** Unflatten a list of UTBSInstitution objects (done in place). */
    protected static void unflatten(EntityMap               em,
                                    List<UTBSInstitution>   institutions)
    {
        if (institutions != null)
            for (int i=0; i<institutions.size(); i++)
                institutions.set(i, institutions.get(i).unflatten(em));
    }
}
