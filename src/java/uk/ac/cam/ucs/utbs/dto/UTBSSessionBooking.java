/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.utbs.dto;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Class representing the state of a booking as related to a single session
 * of an event returned by the web service API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class UTBSSessionBooking
{
    /** The session number. */
    @XmlAttribute public Integer sessionNumber;

    /** Whether or not the session was booked. */
    @XmlAttribute public Boolean booked;

    /** Whether or not the participant attended the session. */
    @XmlAttribute public Boolean attended;

    /**
     * Whether or not the participant passed the session (for assessed
     * events).
     */
    @XmlAttribute public Boolean passed;
}
