/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.utbs.dto;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import uk.ac.cam.ucs.utbs.dto.UTBSResult.EntityMap;

/**
 * Class representing a booking on an event returned by the web service API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class UTBSEventBooking
{
    /** The unique identifier of the booking. */
    @XmlAttribute public Long bookingId;

    /**
     * The event booked. Only the event ID field will be populated unless the
     * <code>fetch</code> parameter included the "event" option.
     */
    @XmlElement public UTBSEvent event;

    /**
     * The person the booking is for. Only the CRSid field will be populated
     * unless the <code>fetch</code> parameter included the "participant"
     * option.
     */
    @XmlElement public UTBSPerson participant;

    /**
     * The person who made the booking. Only the CRSid field will be
     * populated unless the <code>fetch</code> parameter included the
     * "booked_by" option.
     */
    @XmlElement public UTBSPerson bookedBy;

    /** The time that the booking was originally made. */
    @XmlElement public Date bookingTime;

    /**
     * The status of the booking ("provisional", "booked", "standby",
     * "offered", "no response", "waiting", "interested", "cancelled",
     * "completed" or "did not book"). Confirmed bookings will have a status
     * of "booked".
     * <p>
     * For events with optional sessions, the set of sessions booked is
     * recorded in {@link #sessionBookings}.
     */
    @XmlElement public String status;

    /**
     * The participant's institution chosen at the time the booking was made.
     * This institution may no longer be the participant's institution, and
     * it may now no longer exist (i.e., it may be cancelled). Only the
     * {@link UTBSInstitution#instid instid} field will be populated unless
     * the <code>fetch</code> parameter included the "institution" option.
     */
    @XmlElement public UTBSInstitution institution;

    /**
     * The staff/student status of the participant at the time the booking
     * was made. For members of the University of Cambridge, this will be one
     * of the following: "staff", "student", "staff,student" or "". For
     * external participants, it will be null.
     */
    @XmlElement public String misStatus;

    /** Any special requirements the participant has. */
    @XmlElement public String specialRequirements;

    /**
     * Whether or not the participant attended all the non-optional sessions
     * of the event. This will be null until attendance has been recorded for
     * all sessions. Attendance on individual sessions is recorded in
     * {@link #sessionBookings}.
     */
    @XmlAttribute public Boolean attended;

    /**
     * Whether or not the participant passed all the non-optional sessions
     * of an assessed event. This will be null for events that are not
     * assessed. Whether or not individual sessions were passed is recorded
     * in {@link #sessionBookings}.
     */
    @XmlAttribute public Boolean passed;

    /**
     * Whether or not the participant was satisfied with the event. This will
     * be null if user feedback was not recorded.
     */
    @XmlAttribute public Boolean satisfied;

    /**
     * A list of the session booking details. This records the individual
     * sessions that were booked, as well as sessions attended and (for
     * assessed events) sessions passed. This will only be populated if the
     * <code>fetch</code> parameter included the "session_bookings" option.
     */
    @XmlElementWrapper
    @XmlElement(name = "sessionBooking")
    public List<UTBSSessionBooking> sessionBookings;

    /**
     * A list of all the participant's institutions, as they were at the time
     * the booking was made. This may include now-cancelled institutions.
     * This will only be populated if the <code>fetch</code> parameter
     * included the "institutions" option.
     */
    @XmlElementWrapper
    @XmlElement(name = "institution")
    public List<UTBSInstitution> institutions;

    /* == Attributes/methods for the flattened XML/JSON representation == */

    /**
     * An ID that can uniquely identify this booking within the returned
     * XML/JSON document. This is only used in the flattened XML/JSON
     * representation (if the "flatten" parameter is specified).
     */
    @XmlAttribute public String id;

    /**
     * A reference (by id) to a booking element in the XML/JSON document.
     * This is only used in the flattened XML/JSON representation (if the
     * "flatten" parameter is specified).
     */
    @XmlAttribute public String ref;

    /** Compute an ID that can uniquely identify this entity in XML/JSON. */
    public String id() { return "booking/"+bookingId; }

    /** Return a new UTBSEventBooking that refers to this booking. */
    public UTBSEventBooking ref()
    {
        UTBSEventBooking b = new UTBSEventBooking();
        b.ref = id;
        return b;
    }

    /* == Code to help clients unflatten a UTBSEventBooking object == */

    /* Flag to prevent infinite recursion due to circular references. */
    private boolean unflattened;

    /** Unflatten a single UTBSEventBooking. */
    protected UTBSEventBooking unflatten(EntityMap  em)
    {
        if (ref != null)
        {
            UTBSEventBooking booking = em.getBooking(ref);
            if (!booking.unflattened)
            {
                booking.unflattened = true;
                if (booking.event != null)
                    booking.event = booking.event.unflatten(em);
                if (booking.participant != null)
                    booking.participant = booking.participant.unflatten(em);
                if (booking.bookedBy != null)
                    booking.bookedBy = booking.bookedBy.unflatten(em);
                if (booking.institution != null)
                    booking.institution = booking.institution.unflatten(em);
                UTBSInstitution.unflatten(em, booking.institutions);
            }
            return booking;
        }
        return this;
    }

    /** Unflatten a list of UTBSEventBooking objects (done in place). */
    protected static void unflatten(EntityMap               em,
                                    List<UTBSEventBooking>  bookings)
    {
        if (bookings != null)
            for (int i=0; i<bookings.size(); i++)
                bookings.set(i, bookings.get(i).unflatten(em));
    }
}
