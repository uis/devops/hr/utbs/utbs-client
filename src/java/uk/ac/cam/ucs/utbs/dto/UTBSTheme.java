/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.utbs.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import uk.ac.cam.ucs.utbs.dto.UTBSResult.EntityMap;

/**
 * Class representing a theme returned by the web service API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class UTBSTheme
{
    /** The unique internal identifier of the theme. */
    @XmlAttribute public Long themeId;

    /**
     * The theme's provider. This will only be populated if the
     * <code>fetch</code> parameter included the "provider" option.
     */
    @XmlElement public UTBSProvider provider;

    /** The theme's short name (as it appears in some URLs). */
    @XmlElement public String shortName;

    /** The theme's title (as it appears in most of the UTBS). */
    @XmlElement public String title;

    /**
     * A list of the theme's courses. This will only be populated if the
     * <code>fetch</code> parameter included the "courses" option.
     */
    @XmlElementWrapper
    @XmlElement(name = "course")
    public List<UTBSCourse> courses;

    /**
     * A list of all the theme's events. This will only be populated if the
     * <code>fetch</code> parameter included the "events" option.
     */
    @XmlElementWrapper
    @XmlElement(name = "event")
    public List<UTBSEvent> events;

    /* == Attributes/methods for the flattened XML/JSON representation == */

    /**
     * An ID that can uniquely identify this theme within the returned
     * XML/JSON document. This is only used in the flattened XML/JSON
     * representation (if the "flatten" parameter is specified).
     */
    @XmlAttribute public String id;

    /**
     * A reference (by id) to a theme element in the XML/JSON document.
     * This is only used in the flattened XML/JSON representation (if the
     * "flatten" parameter is specified).
     */
    @XmlAttribute public String ref;

    /** Compute an ID that can uniquely identify this entity in XML/JSON. */
    public String id() { return "theme/"+themeId; }

    /** Return a new UTBSTheme that refers to this theme. */
    public UTBSTheme ref() { UTBSTheme t = new UTBSTheme(); t.ref = id; return t; }

    /* == Code to help clients unflatten a UTBSTheme object == */

    /* Flag to prevent infinite recursion due to circular references. */
    private boolean unflattened;

    /** Unflatten a single UTBSTheme. */
    protected UTBSTheme unflatten(EntityMap em)
    {
        if (ref != null)
        {
            UTBSTheme theme = em.getTheme(ref);
            if (!theme.unflattened)
            {
                theme.unflattened = true;
                if (theme.provider != null)
                    theme.provider = theme.provider.unflatten(em);
                UTBSCourse.unflatten(em, theme.courses);
                UTBSEvent.unflatten(em, theme.events);
            }
            return theme;
        }
        return this;
    }

    /** Unflatten a list of UTBSTheme objects (done in place). */
    protected static void unflatten(EntityMap       em,
                                    List<UTBSTheme> themes)
    {
        if (themes != null)
            for (int i=0; i<themes.size(); i++)
                themes.set(i, themes.get(i).unflatten(em));
    }
}
