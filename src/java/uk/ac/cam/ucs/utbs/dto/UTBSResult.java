/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.utbs.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Class representing the top-level container for all XML and JSON results.
 * This may be just a simple textual value or it may contain more complex
 * entities such as providers, programmes, events, courses, themes, venues,
 * people, bookings or institutions.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
@XmlRootElement(name = "result")
public class UTBSResult
{
    /** The web service API version number. */
    @XmlAttribute public String version;

    /** The value returned by methods that return a simple textual value. */
    @XmlElement public String value;

    /**
     * The provider returned by methods that return a single provider.
     * <p>
     * Note that methods that may return multiple providers will always use
     * the {@link #providers} field, even if only one provider is returned.
     */
    @XmlElement public UTBSProvider provider;

    /**
     * The programme returned by methods that return a single programme.
     * <p>
     * Note that methods that may return multiple programmes will always use
     * the {@link #programmes} field, even if only one programme is returned.
     */
    @XmlElement public UTBSProgramme programme;

    /**
     * The event returned by methods that return a single event.
     * <p>
     * Note that methods that may return multiple events will always use the
     * {@link #events} field, even if only one event is returned.
     */
    @XmlElement public UTBSEvent event;

    /**
     * The course returned by methods that return a single course.
     * <p>
     * Note that methods that may return multiple courses will always use the
     * {@link #courses} field, even if only one course is returned.
     */
    @XmlElement public UTBSCourse course;

    /**
     * The theme returned by methods that return a single theme.
     * <p>
     * Note that methods that may return multiple themes will always use the
     * {@link #themes} field, even if only one theme is returned.
     */
    @XmlElement public UTBSTheme theme;

    /**
     * The venue returned by methods that return a single venue.
     * <p>
     * Note that methods that may return multiple venues will always use the
     * {@link #venues} field, even if only one venue is returned.
     */
    @XmlElement public UTBSVenue venue;

    /**
     * The person returned by methods that return a single person.
     * <p>
     * Note that methods that may return multiple people will always use the
     * {@link #people} field, even if only one person is returned.
     */
    @XmlElement public UTBSPerson person;

    /**
     * The booking returned by methods that return a single booking.
     * <p>
     * Note that methods that may return multiple bookings will always use
     * the {@link #bookings} field, even if only one booking is returned.
     */
    @XmlElement public UTBSEventBooking booking;

    /**
     * The institution returned by methods that return a single institution.
     * <p>
     * Note that methods that may return multiple institutions will always
     * use the {@link #institutions} field, even if only one institution is
     * returned.
     */
    @XmlElement public UTBSInstitution institution;

    /** If the method failed, details of the error. */
    @XmlElement public UTBSError error;

    /**
     * The list of providers returned by methods that may return multiple
     * providers. This may be empty, or contain one or more providers.
     */
    @XmlElementWrapper
    @XmlElement(name = "provider")
    public List<UTBSProvider> providers;

    /**
     * The list of programmes returned by methods that may return multiple
     * programmes. This may be empty, or contain one or more programmes.
     */
    @XmlElementWrapper
    @XmlElement(name = "programme")
    public List<UTBSProgramme> programmes;

    /**
     * The list of events returned by methods that may return multiple
     * events. This may be empty, or contain one or more events.
     */
    @XmlElementWrapper
    @XmlElement(name = "event")
    public List<UTBSEvent> events;

    /**
     * The list of courses returned by methods that may return multiple
     * courses. This may be empty, or contain one or more courses.
     */
    @XmlElementWrapper
    @XmlElement(name = "course")
    public List<UTBSCourse> courses;

    /**
     * The list of themes returned by methods that may return multiple
     * themes. This may be empty, or contain one or more themes.
     */
    @XmlElementWrapper
    @XmlElement(name = "theme")
    public List<UTBSTheme> themes;

    /**
     * The list of venues returned by methods that may return multiple
     * venues. This may be empty, or contain one or more venues.
     */
    @XmlElementWrapper
    @XmlElement(name = "venue")
    public List<UTBSVenue> venues;

    /**
     * The list of people returned by methods that may return multiple
     * people. This may be empty, or contain one or more people.
     */
    @XmlElementWrapper
    @XmlElement(name = "person")
    public List<UTBSPerson> people;

    /**
     * The list of bookings returned by methods that may return multiple
     * bookings. This may be empty, or contain one or more bookings.
     */
    @XmlElementWrapper
    @XmlElement(name = "booking")
    public List<UTBSEventBooking> bookings;

    /**
     * The list of institutions returned by methods that may return multiple
     * institutions. This may be empty, or contain one or more institutions.
     */
    @XmlElementWrapper
    @XmlElement(name = "institution")
    public List<UTBSInstitution> institutions;

    /**
     * In the flattened XML/JSON representation, all the unique entities
     * returned by the method.
     * <p>
     * NOTE: This will be null unless the "flatten" parameter is true.
     */
    @XmlElement public Entities entities;

    /** No-arg constructor required by JAXB */
    public UTBSResult() { }

    /** More useful constructor, for purely textual results */
    public UTBSResult(String value) { this.value = value; }

    @Override
    public String toString() { return value; }

    /**
     * Nested class to hold the full details of all the entities returned
     * in a result. This is used only in the flattened result representation,
     * where each of these entities will have a unique textual ID, and be
     * referred to from the top-level objects returned (and by each other).
     * <p>
     * In the hierarchical representation, this is not used, since all
     * entities returned will be at the top-level, or directly contained in
     * those top-level entities.
     */
    public static class Entities
    {
        /**
         * A list of all the unique providers returned by the method. This
         * may include additional providers returned as a result of the
         * <code>fetch</code> parameter, so this list may contain more
         * entries than the corresponding field on the enclosing class.
         */
        @XmlElementWrapper
        @XmlElement(name = "provider")
        public List<UTBSProvider> providers;

        /**
         * A list of all the unique programmes returned by the method. This
         * may include additional programmes returned as a result of the
         * <code>fetch</code> parameter, so this list may contain more
         * entries than the corresponding field on the enclosing class.
         */
        @XmlElementWrapper
        @XmlElement(name = "programme")
        public List<UTBSProgramme> programmes;

        /**
         * A list of all the unique events returned by the method. This may
         * include additional events returned as a result of the
         * <code>fetch</code> parameter, so this list may contain more
         * entries than the corresponding field on the enclosing class.
         */
        @XmlElementWrapper
        @XmlElement(name = "event")
        public List<UTBSEvent> events;

        /**
         * A list of all the unique courses returned by the method. This may
         * include additional courses returned as a result of the
         * <code>fetch</code> parameter, so this list may contain more
         * entries than the corresponding field on the enclosing class.
         */
        @XmlElementWrapper
        @XmlElement(name = "course")
        public List<UTBSCourse> courses;

        /**
         * A list of all the unique themes returned by the method. This may
         * include additional themes returned as a result of the
         * <code>fetch</code> parameter, so this list may contain more
         * entries than the corresponding field on the enclosing class.
         */
        @XmlElementWrapper
        @XmlElement(name = "theme")
        public List<UTBSTheme> themes;

        /**
         * A list of all the unique venues returned by the method. This may
         * include additional venues returned as a result of the
         * <code>fetch</code> parameter, so this list may contain more
         * entries than the corresponding field on the enclosing class.
         */
        @XmlElementWrapper
        @XmlElement(name = "venue")
        public List<UTBSVenue> venues;

        /**
         * A list of all the unique people returned by the method. This may
         * include additional people returned as a result of the
         * <code>fetch</code> parameter, so this list may contain more
         * entries than the corresponding field on the enclosing class.
         */
        @XmlElementWrapper
        @XmlElement(name = "person")
        public List<UTBSPerson> people;

        /**
         * A list of all the unique bookings returned by the method. This may
         * include additional bookings returned as a result of the
         * <code>fetch</code> parameter, so this list may contain more
         * entries than the corresponding field on the enclosing class.
         */
        @XmlElementWrapper
        @XmlElement(name = "booking")
        public List<UTBSEventBooking> bookings;

        /**
         * A list of all the unique institutions returned by the method. This
         * may include additional institutions returned as a result of the
         * <code>fetch</code> parameter, so this list may contain more
         * entries than the corresponding field on the enclosing class.
         */
        @XmlElementWrapper
        @XmlElement(name = "institution")
        public List<UTBSInstitution> institutions;

        /** Add a provider to the list of providers, creating it if necessary */
        public void addProvider(UTBSProvider provider)
        {
            if (providers == null) providers = new ArrayList<UTBSProvider>();
            provider.id = provider.id();
            providers.add(provider);
        }

        /** Add a programme to the list of programmes, creating it if necessary */
        public void addProgramme(UTBSProgramme programme)
        {
            if (programmes == null) programmes = new ArrayList<UTBSProgramme>();
            programme.id = programme.id();
            programmes.add(programme);
        }

        /** Add an event to the list of events, creating it if necessary */
        public void addEvent(UTBSEvent event)
        {
            if (events == null) events = new ArrayList<UTBSEvent>();
            event.id = event.id();
            events.add(event);
        }

        /** Add a course to the list of courses, creating it if necessary */
        public void addCourse(UTBSCourse course)
        {
            if (courses == null) courses = new ArrayList<UTBSCourse>();
            course.id = course.id();
            courses.add(course);
        }

        /** Add a theme to the list of themes, creating it if necessary */
        public void addTheme(UTBSTheme theme)
        {
            if (themes == null) themes = new ArrayList<UTBSTheme>();
            theme.id = theme.id();
            themes.add(theme);
        }

        /** Add a venue to the list of venues, creating it if necessary */
        public void addVenue(UTBSVenue venue)
        {
            if (venues == null) venues = new ArrayList<UTBSVenue>();
            venue.id = venue.id();
            venues.add(venue);
        }

        /** Add a person to the list of people, creating it if necessary */
        public void addPerson(UTBSPerson person)
        {
            if (people == null) people = new ArrayList<UTBSPerson>();
            person.id = person.id();
            people.add(person);
        }

        /** Add a booking to the list of bookings, creating it if necessary */
        public void addBooking(UTBSEventBooking booking)
        {
            if (bookings == null) bookings = new ArrayList<UTBSEventBooking>();
            booking.id = booking.id();
            bookings.add(booking);
        }

        /** Add an institution to the list of institutions, creating it if necessary */
        public void addInstitution(UTBSInstitution institution)
        {
            if (institutions == null) institutions = new ArrayList<UTBSInstitution>();
            institution.id = institution.id();
            institutions.add(institution);
        }
    }

    /* == Code to help clients unflatten a UTBSResult object == */

    /**
     * Nested class to assist during the unflattening process, maintaining
     * efficient maps from IDs to entities (providers, programmes, events,
     * courses, themes, venues, people, bookings and institutions).
     */
    @XmlTransient
    protected static class EntityMap
    {
        private Map<String, UTBSProvider>       providersById;
        private Map<String, UTBSProgramme>      progsById;
        private Map<String, UTBSEvent>          eventsById;
        private Map<String, UTBSCourse>         coursesById;
        private Map<String, UTBSTheme>          themesById;
        private Map<String, UTBSVenue>          venuesById;
        private Map<String, UTBSPerson>         peopleById;
        private Map<String, UTBSEventBooking>   bookingsById;
        private Map<String, UTBSInstitution>    institutionsById;

        /** Construct an entity map from a flattened UTBSResult. */
        private EntityMap(UTBSResult result)
        {
            providersById    = new HashMap<String, UTBSProvider>();
            progsById        = new HashMap<String, UTBSProgramme>();
            eventsById       = new HashMap<String, UTBSEvent>();
            coursesById      = new HashMap<String, UTBSCourse>();
            themesById       = new HashMap<String, UTBSTheme>();
            venuesById       = new HashMap<String, UTBSVenue>();
            peopleById       = new HashMap<String, UTBSPerson>();
            bookingsById     = new HashMap<String, UTBSEventBooking>();
            institutionsById = new HashMap<String, UTBSInstitution>();

            if (result.entities.providers != null)
                for (UTBSProvider provider : result.entities.providers)
                    providersById.put(provider.id, provider);
            if (result.entities.programmes != null)
                for (UTBSProgramme programme : result.entities.programmes)
                    progsById.put(programme.id, programme);
            if (result.entities.events != null)
                for (UTBSEvent event : result.entities.events)
                    eventsById.put(event.id, event);
            if (result.entities.courses != null)
                for (UTBSCourse course : result.entities.courses)
                    coursesById.put(course.id, course);
            if (result.entities.themes != null)
                for (UTBSTheme theme : result.entities.themes)
                    themesById.put(theme.id, theme);
            if (result.entities.venues != null)
                for (UTBSVenue venue : result.entities.venues)
                    venuesById.put(venue.id, venue);
            if (result.entities.people != null)
                for (UTBSPerson person : result.entities.people)
                    peopleById.put(person.id, person);
            if (result.entities.bookings != null)
                for (UTBSEventBooking booking : result.entities.bookings)
                    bookingsById.put(booking.id, booking);
            if (result.entities.institutions != null)
                for (UTBSInstitution institution : result.entities.institutions)
                    institutionsById.put(institution.id, institution);
        }

        /** Get a provider from the entity map, given its ID */
        protected UTBSProvider getProvider(String id) { return providersById.get(id); }

        /** Get a programme from the entity map, given its ID */
        protected UTBSProgramme getProgramme(String id) { return progsById.get(id); }

        /** Get an event from the entity map, given its ID */
        protected UTBSEvent getEvent(String id) { return eventsById.get(id); }

        /** Get a course from the entity map, given its ID */
        protected UTBSCourse getCourse(String id) { return coursesById.get(id); }

        /** Get a theme from the entity map, given its ID */
        protected UTBSTheme getTheme(String id) { return themesById.get(id); }

        /** Get a venue from the entity map, given its ID */
        protected UTBSVenue getVenue(String id) { return venuesById.get(id); }

        /** Get a person from the entity map, given its ID */
        protected UTBSPerson getPerson(String id) { return peopleById.get(id); }

        /** Get a booking from the entity map, given its ID */
        protected UTBSEventBooking getBooking(String id) { return bookingsById.get(id); }

        /** Get an institution from the entity map, given its ID */
        protected UTBSInstitution getInstitution(String id) { return institutionsById.get(id); }
    }

    /**
     * Unflatten this UTBSResult object, resolving any internal ID refs to
     * build a fully fledged object tree.
     * <p>
     * This is necessary if the UTBSResult was constructed from XML/JSON in
     * its flattened representation (with the "flatten" parameter set to
     * true).
     * <p>
     * On entry, the UTBSResult object may have providers, programmes,
     * events, courses, themes, venues, people, bookings or institutions in
     * it with "ref" fields referring to objects held in the "entities"
     * lists. After unflattening, all such references will have been replaced
     * by actual object references, giving an object tree that can be
     * traversed normally.
     *
     * @return This UTBSResult object, with its internals unflattened.
     */
    public UTBSResult unflatten()
    {
        if (entities != null)
        {
            EntityMap em = new EntityMap(this);

            if (provider != null)
                provider = provider.unflatten(em);
            if (programme != null)
                programme = programme.unflatten(em);
            if (event != null)
                event = event.unflatten(em);
            if (course != null)
                course = course.unflatten(em);
            if (theme != null)
                theme = theme.unflatten(em);
            if (venue != null)
                venue = venue.unflatten(em);
            if (person != null)
                person = person.unflatten(em);
            if (booking != null)
                booking = booking.unflatten(em);
            if (institution != null)
                institution = institution.unflatten(em);

            UTBSProvider.unflatten(em, providers);
            UTBSProgramme.unflatten(em, programmes);
            UTBSEvent.unflatten(em, events);
            UTBSCourse.unflatten(em, courses);
            UTBSTheme.unflatten(em, themes);
            UTBSVenue.unflatten(em, venues);
            UTBSPerson.unflatten(em, people);
            UTBSEventBooking.unflatten(em, bookings);
            UTBSInstitution.unflatten(em, institutions);
        }
        return this;
    }
}
