# --------------------------------------------------------------------------
# Copyright (c) 2013, University of Cambridge Computing Service.
#
# This file is part of the University Training Booking System client library.
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Dean Rasheed (dev-group@ucs.cam.ac.uk)
# --------------------------------------------------------------------------

"""
Connection classes to connect to the UTBS web service and allow API methods
to be invoked.
"""

import base64
from datetime import date, datetime, timedelta, tzinfo
from http.client import HTTPSConnection
import socket
import os
import urllib.parse

from .dto import UTBSDto, UTBSError, UTBSResult, UTBSResultParser

try:
    import ssl
    _have_ssl = True
except ImportError:
    print("WARNING: No SSL support - connection may be insecure")
    _have_ssl = False

# Use the latest TLS protocol supported by both the client and the server.
# Prior to Python versions 2.7.13 and 3.6, the way to do that was by
# specifying PROTOCOL_SSLv23. In more recent versions, that is deprecated,
# and presumably might go away one day. Instead, the new value PROTOCOL_TLS
# has been added, and is now the recommended way to do it. This is actually
# the same constant (2). Note that, despite the name, this will never choose
# insecure SSL v2 or v3 protocols, because those are disabled on the server.
if hasattr(ssl, 'PROTOCOL_TLS'):
    _ssl_protocol = ssl.PROTOCOL_TLS
else:
    _ssl_protocol = ssl.PROTOCOL_SSLv23

_MONTHS = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
           "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]

class TimeZone(tzinfo):
    """
    Custom timezone class, repesenting an offset in hours and/or minutes.

    This may be used to create a timezone aware datetime, for example the
    following in British Summer Time:

    .. code-block:: python

      >>> dt = datetime(2005, 6, 1, 14, 33, 51, 0, TimeZone(hours=1))
      >>> dt.strftime('%Y-%m-%dT%H:%M:%S%Z')
      '2005-06-01T14:33:51+01:00'
    """
    def __init__(self, hours=0, minutes=0):
        offset = int(hours * 60 + minutes)
        self.__offset = timedelta(minutes = offset)
        self.__dst = timedelta(0)
        self.__name = ("%+d"%offset)[0] +\
                      "%02d:%02d" % (abs(offset)/60, abs(offset)%60)
    def utcoffset(self, dt):
        return self.__offset
    def dst(self, dt):
        return self.__dst
    def tzname(self, dt):
        return self.__name

class UTBSException(Exception):
    """
    Exception thrown when a web service API method fails. This is wrapper
    around the :any:`UTBSError` object returned by the server, which contains
    the full details of what went wrong.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    def __init__(self, error):
        Exception.__init__(self, error.message)
        self.error = error

    def get_error(self):
        """
        Returns the underlying error from the server.

        **Returns**
          :any:`UTBSError`
            The underlying error from the server.
        """
        return self.error

class HTTPSValidatingConnection(HTTPSConnection):
    """
    Class extending the standard :py:class:`HTTPSConnection` class from
    :any:`http.client`, so that it checks the server's certificates,
    validating them against the specified CA certificates.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    def __init__(self, host, port, ca_certs):
        HTTPSConnection.__init__(self, host, port)
        self.ca_certs = ca_certs

    def connect(self):
        """
        Overridden connect() method to wrap the socket using an SSLSocket,
        and check the server certificates.
        """
        try:
            self.sock = socket.create_connection((self.host, self.port))
        except AttributeError:
            HTTPSConnection.connect(self)

        if not _have_ssl:
            # No SSL available - insecure connection
            print("WARNING: No SSL support - connection may be insecure")
        elif self.ca_certs:
            # Wrap the socket in an SSLSocket, and tell it to validate
            # the server certificates. Note that this does not check that
            # the certificate's host matches, so we must do that ourselves.
            self.sock = ssl.wrap_socket(self.sock,
                                        ca_certs = self.ca_certs,
                                        cert_reqs = ssl.CERT_REQUIRED,
                                        ssl_version = _ssl_protocol)

            cert = self.sock.getpeercert()
            cert_hosts = []
            host_valid = False

            if "subject" in cert:
                for x in cert["subject"]:
                    if x[0][0] == "commonName":
                        cert_hosts.append(x[0][1])
            if "subjectAltName" in cert:
                for x in cert["subjectAltName"]:
                    if x[0] == "dns":
                        cert_hosts.append(x[1])

            for cert_host in cert_hosts:
                if self.host.startswith(cert_host):
                    host_valid = True

            if not host_valid:
                raise ssl.SSLError("Host name '%s' doesn't match "\
                                   "certificate host %s"\
                                   % (self.host, str(cert_hosts)))
        else:
            # No CA certificates supplied, so can't validate the server
            # certificates, but we still wrap the socket in an SSLSocket
            # so that all data is encrypted.
            self.sock = ssl.wrap_socket(self.sock,
                                        ca_certs = None,
                                        cert_reqs = ssl.CERT_NONE,
                                        ssl_version = _ssl_protocol)

class UTBSClientConnection:
    """
    Class to connect to the UTBS server and invoke web service API methods.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    def __init__(self, host, port, url_base, check_certs):
        self.host = host
        self.port = port
        self.url_base = url_base

        if not self.url_base.startswith("/"):
            self.url_base = "/%s" % self.url_base
        if not self.url_base.endswith("/"):
            self.url_base = "%s/" % self.url_base

        if check_certs:
            utbsclient_dir = os.path.realpath(os.path.dirname(__file__))
            self.ca_certs = os.path.join(utbsclient_dir, "cacerts.txt")
        else:
            self.ca_certs = None

        self.username = None
        self.password = None
        self.authorization = None

    def _update_authorization(self):
        if self.username:
            credentials = "%s:%s" % (self.username, self.password)
            credential_bytes = bytes(credentials, "UTF-8")
            base64_credentials = str(base64.b64encode(credential_bytes), "UTF-8")
            self.authorization = "Basic %s" % base64_credentials
        else:
            self.authorization = None

    def set_username(self, username):
        """
        Set the username to use when connecting to the UTBS web service. By
        default connections are anonymous, which gives read-only access to a
        limited set of data. Certain API methods, however, require
        authentication as a user with elevated privileges, using the person's
        API password.

        This method may be called at any time, and affects all subsequent
        access using this connection, but should not affect any other
        :any:`UTBSClientConnection` objects.

        **Parameters**
          `username` : str
            [required] The CRSid of the user to connect as.
        """
        self.username = username
        self._update_authorization()

    def set_password(self, password):
        """
        Set the password to use when connecting to the UTBS web service. This
        is only necessary when authenticating using
        :any:`set_username() <UTBSClientConnection.set_username>`, in which
        case the password should be the user's API password.

        **Parameters**
          `password` : str
            [required] The API password of the user.
        """
        self.password = password
        self._update_authorization()

    def _params_to_strings(self, params):
        """
        Convert the values in a parameter map into strings suitable for
        sending to the server. Any null values will be omitted.
        """
        new_params = {}
        for key, value in params.items():
            if value != None:
                if isinstance(value, bool):
                    if value: new_params[key] = "true"
                    else: new_params[key] = "false"
                elif isinstance(value, datetime):
                    new_params[key] = value.strftime('%Y-%m-%dT%H:%M:%S%Z')
                elif isinstance(value, date):
                    new_params[key] = value.strftime('%Y-%m-%d')
                elif isinstance(value, list) or isinstance(value, tuple):
                    new_params[key] = ",".join(value)
                elif isinstance(value, UTBSDto):
                    new_params[key] = value.encoded_string()
                elif not isinstance(value, str):
                    new_params[key] = str(value)
                else:
                    new_params[key] = value

        return new_params

    def _build_url(self, path, path_params={}, query_params={}):
        """
        Build the full URL needed to invoke a method in the web service API.

        The path may contain standard Python format specifiers, which will
        be substituted from the path parameters (suitably URL-encoded). Thus
        for example, given the following arguments:

            * path = "api/v1/method/%(param1)s/%(param2)s"
            * path_params = {"param1": "foo", "param2": "bar"}
            * query_params = {"fetch": "this,that"}

        this method will create a URL like the following:

            api/v1/method/foo/bar?fetch=this%2Cthat

        Note that all parameter values are automatically URL-encoded.
        """
        for key, value in path_params.items():
            path_params[key] = urllib.parse.quote_plus(value)
        path = path % path_params

        if "flatten" not in query_params:
            query_params["flatten"] = "true"
        path += "?%s" % urllib.parse.urlencode(query_params)

        if path.startswith("/"):
            return "%s%s" % (self.url_base, path[1:])
        return "%s%s" % (self.url_base, path)

    def invoke_method(self, method, path, path_params={},
                      query_params={}, form_params={}):
        """
        Invoke a web service GET, POST, PUT or DELETE method.

        The path should be the relative path to the method with standard
        Python format specifiers for any path parameters, for example
        ``"/api/v1/method/%(param1)s/%(param2)s"``. Any path parameters
        specified are then substituted into the path.

        **Parameters**
          `method` : str
            [required] The method type (``"GET"``, ``"POST"``, ``"PUT"`` or
            ``"DELETE"``).

          `path` : str
            [required] The path to the method to invoke.

          `path_params` : dict
            [optional] Any path parameters that should be inserted into the
            path in place of any format specifiers.

          `query_params` : dict
            [optional] Any query parameters to add as part of the URL's query
            string.

          `form_params` : dict
            [optional] Any form parameters to submit.

        **Returns**
          :any:`UTBSResult`
            The result of invoking the method.
        """
        path_params = self._params_to_strings(path_params)
        query_params = self._params_to_strings(query_params)
        form_params = self._params_to_strings(form_params)

        conn = HTTPSValidatingConnection(self.host, self.port, self.ca_certs)
        url = self._build_url(path, path_params, query_params)
        headers = {"Accept": "application/xml"}
        if self.authorization:
            headers["Authorization"] = self.authorization

        if form_params:
            body = urllib.parse.urlencode(form_params)
            conn.request(method, url, body, headers)
        else:
            conn.request(method, url, headers=headers)

        response = conn.getresponse()
        content_type = response.getheader("Content-type")
        if content_type != "application/xml":
            error = UTBSError({"status": response.status,
                               "code": response.reason})
            error.message = "Unexpected result from server"
            error.details = response.read()

            result = UTBSResult()
            result.error = error

            return result

        parser = UTBSResultParser()
        result = parser.parse_xml(response.read())
        conn.close()

        return result

def createConnection():
    """
    Create a UTBSClientConnection to the UTBS web service API at
    https://www.training.cam.ac.uk/.

    The connection is initially anonymous (not authenticated), but this may
    be changed using its
    :any:`set_username() <UTBSClientConnection.set_username>` and
    :any:`set_password() <UTBSClientConnection.set_password>` methods.

    **Returns**
      :any:`UTBSClientConnection`
        A new connection to the UTBS server.
    """
    return UTBSClientConnection("www.training.cam.ac.uk", 443, "", True)

def createTestConnection():
    """
    Create a UTBSClientConnection to the UTBS test web service API
    https://utbs-test.csx.cam.ac.uk/.

    The connection is initially anonymous (not authenticated), but this may
    be changed using its
    :any:`set_username() <UTBSClientConnection.set_username>` and
    :any:`set_password() <UTBSClientConnection.set_password>` methods.

    .. note::
      This test server is not guaranteed to always be available, and the data
      in it differs considerably from the data on the live system.

    **Returns**
      :any:`UTBSClientConnection`
        A new connection to the UTBS test server.
    """
    return UTBSClientConnection("utbs-test.csx.cam.ac.uk", 443, "", True)

def createLocalConnection():
    """
    Create a UTBSClientConnection to a UTBS web service API running locally
    on https://localhost:8443/course-booking/.

    The connection is initially anonymous (not authenticated), but this may
    be changed using its
    :any:`set_username() <UTBSClientConnection.set_username>` and
    :any:`set_password() <UTBSClientConnection.set_password>` methods.

    This is intended for testing during development. The local server is
    assumed to be using self-signed certificates, which will not be checked.

    **Returns**
      :any:`UTBSClientConnection`
        A new connection to a UTBS server assumed to be running on localhost.
    """
    return UTBSClientConnection("localhost", 8443, "course-booking", False)
