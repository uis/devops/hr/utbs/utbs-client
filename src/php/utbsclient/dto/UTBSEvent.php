<?php
/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once "UTBSCourse.php";
require_once "UTBSDto.php";
require_once "UTBSEventSession.php";
require_once "UTBSTheme.php";
require_once "UTBSVenue.php";

/**
 * Class representing an event returned by the web service API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
class UTBSEvent extends UTBSDto
{
    /* Properties marked as @XmlAttribute in the JAXB class */
    protected static $xmlAttrs = array("eventId", "newEvent", "updatedEvent",
                                       "specialEvent", "extraRun",
                                       "beginnersEvent", "hasPrerequisites",
                                       "showOnTimetable", "showOnThemesPage",
                                       "id", "ref");

    /* Properties marked as @XmlElement in the JAXB class */
    protected static $xmlElems = array("provider", "programme", "course",
                                       "startDate", "endDate", "title",
                                       "courseType", "subType", "status",
                                       "targetAudience", "description",
                                       "duration", "frequency", "format",
                                       "prerequisites", "aims", "objectives",
                                       "systemRequirements", "notes",
                                       "maxParticipants");

    /* Properties marked as @XmlElementWrapper in the JAXB class */
    protected static $xmlArrays = array("topics", "extraInfo",
                                        "relatedCourses", "themes",
                                        "sessions", "venues", "trainers",
                                        "bookings");

    /** @var int The unique identifier of the event. */
    public $eventId;

    /**
     * @var UTBSProvider The event's provider. This will only be populated if
     * the ``fetch`` parameter included the ``"provider"`` option.
     */
    public $provider;

    /**
     * @var UTBSProgramme The programme to which the event belongs. This will
     * only be populated if the ``fetch`` parameter included the
     * ``"programme"`` option.
     */
    public $programme;

    /** @var UTBSCourse The course that the event is for. */
    public $course;

    /** @var DateTime The start date and time of the event. */
    public $startDate;

    /** @var DateTime The end date and time of the event. */
    public $endDate;

    /** @var string The event's title. */
    public $title;

    /**
     * @var string The event's course type. This will be one of
     * ``"instructor-led"``, ``"self-taught"`` or ``"facility-booking"``.
     */
    public $courseType;

    /**
     * @var string The sub-type of the event. The possible values vary,
     * depending on the event's course type.
     */
    public $subType;

    /**
     * @var string The event's status (``"published"``, ``"completed"``,
     * ``"postponed"`` or ``"cancelled"``).
     */
    public $status;

    /** @var string The event's target audience. */
    public $targetAudience;

    /** @var string The event's description. */
    public $description;

    /** @var string A textual description of the event's duration. */
    public $duration;

    /** @var string How frequently the event is typically run. */
    public $frequency;

    /** @var The format of the event (how it is taught). */
    public $format;

    /** @var string Any prerequisites for the event. */
    public $prerequisites;

    /** @var string The aims of the course. */
    public $aims;

    /** @var string The objectives of the course. */
    public $objectives;

    /**
     * @var string For self-taught courses, any system requirements for
     * participants.
     */
    public $systemRequirements;

    /** @var string Any additional notes about the course. */
    public $notes;

    /** @var boolean A flag indicating whether this is a new course. */
    public $newEvent;

    /**
     * @var boolean A flag indicating whether this course has been recently
     * updated.
     */
    public $updatedEvent;

    /**
     * @var boolean A flag indicating whether this is a special event, such
     * as a one-off course that won't be repeated.
     */
    public $specialEvent;

    /**
     * @var boolean A flag indicating whether this is an extra run event
     * added to meet unexpected demand.
     */
    public $extraRun;

    /**
     * @var boolean A flag indicating whether this is an event for beginners,
     * with no significant prerequisites.
     */
    public $beginnersEvent;

    /**
     * @var boolean A flag indicating whether this course assumes significant
     * prerequisite experience from participants.
     */
    public $hasPrerequisites;

    /**
     * @var boolean A flag indicating whether or not this event is shown on
     * the timetable.
     */
    public $showOnTimetable;

    /**
     * @var boolean A flag indicating whether or not this event is shown on
     * the themes page.
     */
    public $showOnThemesPage;

    /** The maximum number of participants. */
    public $maxParticipants;

    /**
     * @var UTBSEventTopic[] A list of the event's topics. This will only be
     * populated if the ``fetch`` parameter included the ``"topics"`` option.
     */
    public $topics;

    /**
     * @var UTBSEventExtraInfo[] A list of any extra information sections
     * giving further details about the event. This will only be populated if
     * the ``fetch`` parameter included the ``"extra_info"`` option.
     */
    public $extraInfo;

    /**
     * @var UTBSCourse[] A list of the courses related to the event. This
     * will only be populated if the ``fetch`` parameter included the
     * ``"related_courses"`` option.
     */
    public $relatedCourses;

    /**
     * @var UTBSTheme A list of the event's themes. This will only be
     * populated if the ``fetch`` parameter included the ``"themes"`` option.
     */
    public $themes;

    /**
     * @var UTBSEventSession[] A list of the event's sessions. This will only
     * be populated if the ``fetch`` parameter included the ``"sessions"``
     * option.
     */
    public $sessions;

    /**
     * @var UTBSVenue[] A list of the venues that the event is held in
     * (typically just one). This will only be populated if the ``fetch``
     * parameter included the ``"venues"`` option.
     */
    public $venues;

    /**
     * @var UTBSSessionTrainer[] A list of the event's trainers. This will
     * only be populated if the ``fetch`` parameter included the
     * ``"trainers"`` option.
     */
    public $trainers;

    /**
     * @var UTBSEventBooking[] A list of the event's bookings. This will only
     * be populated if the ``fetch`` parameter included the ``"bookings"``
     * option, and is only available to authenticated users with appropriate
     * privileges for the event. This may be an incomplete list, if you do
     * not have permission to view all the event's bookings.
     */
    public $bookings;

    /**
     * @ignore
     * @var string An ID that can uniquely identify this event within the
     * returned XML/JSON document. This is only used in the flattened
     * XML/JSON representation (if the "flatten" parameter is specified).
     */
    public $id;

    /**
     * @ignore
     * @var string A reference (by id) to an event element in the XML/JSON
     * document. This is only used in the flattened XML/JSON representation
     * (if the "flatten" parameter is specified).
     */
    public $ref;

    /* Flag to prevent infinite recursion due to circular references. */
    private $unflattened;

    /**
     * @ignore
     * Create a UTBSEvent from the attributes of an XML node.
     *
     * @param array $attrs The attributes on the XML node.
     */
    public function __construct($attrs=array())
    {
        parent::__construct($attrs);
        if (isset($this->eventId))
            $this->eventId = (int )$this->eventId;
        if (isset($this->newEvent))
            $this->newEvent = strcasecmp($this->newEvent, "true") == 0;
        if (isset($this->updatedEvent))
            $this->updatedEvent = strcasecmp($this->updatedEvent, "true") == 0;
        if (isset($this->specialEvent))
            $this->specialEvent = strcasecmp($this->specialEvent, "true") == 0;
        if (isset($this->extraRun))
            $this->extraRun = strcasecmp($this->extraRun, "true") == 0;
        if (isset($this->beginnersEvent))
            $this->beginnersEvent = strcasecmp($this->beginnersEvent, "true") == 0;
        if (isset($this->hasPrerequisites))
            $this->hasPrerequisites = strcasecmp($this->hasPrerequisites, "true") == 0;
        if (isset($this->showOnTimetable))
            $this->showOnTimetable = strcasecmp($this->showOnTimetable, "true") == 0;
        if (isset($this->showOnThemesPage))
            $this->showOnThemesPage = strcasecmp($this->showOnThemesPage, "true") == 0;
        $this->unflattened = false;
    }

    /**
     * @ignore
     * Overridden end element callback for XML parsing.
     *
     * @param string $tagname The name of the XML element.
     * @param string $data The textual value of the XML element.
     * @return void.
     */
    public function endChildElement($tagname, $data)
    {
        parent::endChildElement($tagname, $data);
        if ($tagname === "startDate" && isset($this->startDate))
            $this->startDate = new DateTime($this->startDate);
        if ($tagname === "endDate" && isset($this->endDate))
            $this->endDate = new DateTime($this->endDate);
        if ($tagname === "maxParticipants" && isset($this->maxParticipants))
            $this->maxParticipants = (int )$this->maxParticipants;
    }

    /**
     * @ignore
     * Unflatten a single UTBSEvent.
     *
     * @param UTBSResultEntityMap $em The mapping from IDs to entities.
     */
    public function unflatten($em)
    {
        if (isset($this->ref))
        {
            $event = $em->getEvent($this->ref);
            if (!$event->unflattened)
            {
                $event->unflattened = true;
                if (isset($event->provider))
                    $event->provider = $event->provider->unflatten($em);
                if (isset($event->programme))
                    $event->programme = $event->programme->unflatten($em);
                if (isset($event->course))
                    $event->course = $event->course->unflatten($em);
                UTBSCourse::unflattenCourses($em, $event->relatedCourses);
                UTBSTheme::unflattenThemes($em, $event->themes);
                UTBSEventSession::unflattenSessions($em, $event->sessions);
                UTBSVenue::unflattenVenues($em, $event->venues);
                UTBSEventBooking::unflattenBookings($em, $event->bookings);
            }
            return $event;
        }
        return $this;
    }

    /**
     * @ignore
     * Unflatten a list of UTBSEvent objects (done in place).
     *
     * @param UTBSResultEntityMap $em The mapping from IDs to entities.
     * @param UTBSEvent[] $events The events to unflatten.
     */
    public static function unflattenEvents($em, &$events)
    {
        if (isset($events))
            foreach ($events as $idx => $event)
                $events[$idx] = $event->unflatten($em);
    }
}
