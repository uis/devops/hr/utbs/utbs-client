<?php
/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once "UTBSDto.php";

/**
 * Class representing the state of a booking as related to a single session
 * of an event returned by the web service API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
class UTBSSessionBooking extends UTBSDto
{
    /* Properties marked as @XmlAttribute in the JAXB class */
    protected static $xmlAttrs = array("sessionNumber", "booked", "attended",
                                       "passed");

    /** @var int The session number. */
    public $sessionNumber;

    /** @var boolean Whether or not the session was booked. */
    public $booked;

    /** @var boolean Whether or not the participant attended the session. */
    public $attended;

    /**
     * @var boolean Whether or not the participant passed the session (for
     * assessed events).
     */
    public $passed;

    /**
     * @ignore
     * Create a UTBSSessionBooking from the attributes of an XML node.
     *
     * @param array $attrs The attributes on the XML node.
     */
    public function __construct($attrs=array())
    {
        parent::__construct($attrs);
        if (isset($this->sessionNumber))
            $this->sessionNumber = (int )$this->sessionNumber;
        if (isset($this->booked))
            $this->booked = strcasecmp($this->booked, "true") == 0;
        if (isset($this->attended))
            $this->attended = strcasecmp($this->attended, "true") == 0;
        if (isset($this->passed))
            $this->passed = strcasecmp($this->passed, "true") == 0;
    }
}
