<?php
/* === AUTO-GENERATED - DO NOT EDIT === */

/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once dirname(__FILE__) . "/../client/UTBSException.php";

/**
 * Methods for querying and manipulating training providers.
 *
 * **The fetch parameter for providers**
 *
 * All methods that return providers also accept an optional
 * ``fetch`` parameter that may be used to request additional
 * information about the providers returned. For more details about the
 * general rules that apply to the ``fetch`` parameter, refer to the
 * {@link EventMethods} documentation.
 *
 * For providers the ``fetch`` parameter may be used to fetch
 * referenced programmes, themes and venues. The following references are
 * supported:
 *
 * * ``"institution"`` - fetches full details about the provider's
 *   institution.
 *
 * * ``"current_programme"`` - fetches the provider's current
 *   programme, if there is one.
 *
 * * ``"programmes"`` - fetches all the provider's programmes, in date
 *   order.
 *
 * * ``"themes"`` - fetches all the provider's themes.
 *
 * * ``"venues"`` - fetches all the venues belonging to the
 *   provider.
 *
 * As with the event ``fetch`` parameter, the references may be used
 * in a chain by using the "dot" notation to fetch additional information
 * about referenced programmes, themes and venues. For example
 * "themes.courses" will fetch all the courses for each of the provider's
 * themes. For more information about what can be fetched from referenced
 * programmes, themes and venues, refer to the documentation for
 * {@link ProgrammeMethods}, {@link ThemeMethods} and {@link VenueMethods}.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
class ProviderMethods
{
    // The connection to the server
    private $conn;

    /**
     * Create a new ProviderMethods object.
     *
     * @param ClientConnection $conn The ClientConnection object to use to
     * invoke methods on the server.
     */
    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    /**
     * Return a list of all training providers.
     *
     * By default, only a few basic details about each provider are returned,
     * but the optional ``fetch`` parameter may be used to fetch
     * additional attributes or references.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/provider/all-providers ]``
     *
     * @param string $fetch [optional] A comma-separated list of any additional
     * details to fetch for each provider.
     *
     * @return UTBSProvider[] The requested providers (in name order).
     */
    public function getAllProviders($fetch=null)
    {
        $pathParams = array();
        $queryParams = array("fetch" => $fetch);
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/provider/all-providers',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->providers;
    }

    /**
     * Get the training provider with the specified short name (e.g., "UCS").
     *
     * By default, only a few basic details about the provider are returned,
     * but the optional ``fetch`` parameter may be used to fetch
     * additional attributes or references.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/provider/{shortName} ]``
     *
     * @param string $shortName [required] The short name of the provider to fetch.
     * @param string $fetch [optional] A comma-separated list of any additional
     * details to fetch.
     *
     * @return UTBSProvider The requested provider or null if it was not found.
     */
    public function getProvider($shortName,
                                $fetch=null)
    {
        $pathParams = array("shortName" => $shortName);
        $queryParams = array("fetch" => $fetch);
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/provider/%1$s',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->provider;
    }

    /**
     * Get the events run by the specified training provider in the specified
     * time period.
     *
     * This will return any events that overlap the specified time period.
     * More specifically, it will return events whose start is less than or
     * equal to the end of the time period, and whose end is greater than or
     * equal to the start of the time period (i.e., all the start and end
     * timestamps are treated inclusively).
     *
     * Optionally, this will also check the event's sessions and exclude any
     * events that have no sessions overlapping the specified time period.
     * This can happen for events with multiple sessions. For example,
     * suppose an event has 2 sessions, one on Monday and the other on
     * Friday, and that the specified time period to search was on Wednesday.
     * Then by default, the event would be returned, because it starts on
     * Monday and ends on Friday, which overlaps the time period being
     * searched, but if session checking is enabled, the event would be
     * excluded.
     *
     * By default, only a few basic details about each event are returned,
     * but the optional ``fetch`` parameter may be used to fetch
     * additional attributes or references.
     *
     * NOTE: When using this API directly via the URL endpoints, date-time
     * parameters should be supplied as either milliseconds since epoch, or
     * as ISO 8601 formatted date or date-time strings.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/provider/{shortName}/events-in-time-period ]``
     *
     * @param string $shortName [required] The short name of the provider.
     * @param DateTime $start [optional] The start of the time period to search. If
     * omitted, this will default to 0:00am today.
     * @param DateTime $end [optional] The end of the time period to search. If
     * omitted, this will default to the first midnight after the start date.
     * @param boolean $checkSessions [optional] If ``true``, check the event
     * sessions, and exclude any events that have no sessions overlapping the
     * the time period being searched.
     * @param string $fetch [optional] A comma-separated list of any additional
     * details to fetch for each event.
     *
     * @return UTBSEvent[] A list of events found, in (start date-time, ID) order.
     */
    public function getEventsInTimePeriod($shortName,
                                          $start=null,
                                          $end=null,
                                          $checkSessions=null,
                                          $fetch=null)
    {
        $pathParams = array("shortName" => $shortName);
        $queryParams = array("start"         => $start,
                             "end"           => $end,
                             "checkSessions" => $checkSessions,
                             "fetch"         => $fetch);
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/provider/%1$s/events-in-time-period',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->events;
    }

    /**
     * Get all the programmes run by the specified training provider.
     *
     * By default, only a few basic details about each programme are
     * returned, but the optional ``fetch`` parameter may be used to
     * fetch additional attributes or references of each programme.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/provider/{shortName}/programmes ]``
     *
     * @param string $shortName [required] The short name of the provider.
     * @param string $fetch [optional] A comma-separated list of any additional
     * details to fetch for each programme.
     *
     * @return UTBSProgramme[] The provider's programmes, in (date, ID) order.
     */
    public function getProgrammes($shortName,
                                  $fetch=null)
    {
        $pathParams = array("shortName" => $shortName);
        $queryParams = array("fetch" => $fetch);
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/provider/%1$s/programmes',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->programmes;
    }

    /**
     * Get the programmes run by the specified training provider in the
     * specified date range.
     *
     * This will return any programmes that overlap the specified date range.
     * More specifically, it will return programmes whose start is less than
     * or equal to the end date specified, and whose end is greater than or
     * equal to the start date specified (i.e., all the start and end dates
     * are treated inclusively).
     *
     * By default, only a few basic details about each programme are
     * returned, but the optional ``fetch`` parameter may be used to
     * fetch additional attributes or references.
     *
     * NOTE: When using this API directly via the URL endpoints, date
     * parameters should be supplied as either milliseconds since epoch, or
     * as ISO 8601 formatted date or date-time strings.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/provider/{shortName}/programmes-by-date ]``
     *
     * @param string $shortName [required] The short name of the provider.
     * @param DateTime $startDate [optional] The start date. If omitted, this will
     * default to today.
     * @param DateTime $endDate [optional] The end date. If omitted, this will default
     * to today.
     * @param string $fetch [optional] A comma-separated list of any additional
     * details to fetch for each programme.
     *
     * @return UTBSProgramme[] A list of programmes found, in (date, ID) order.
     */
    public function getProgrammesByDate($shortName,
                                        $startDate=null,
                                        $endDate=null,
                                        $fetch=null)
    {
        $pathParams = array("shortName" => $shortName);
        $queryParams = array("startDate" => $startDate,
                             "endDate"   => $endDate,
                             "fetch"     => $fetch);
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/provider/%1$s/programmes-by-date',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->programmes;
    }

    /**
     * Get all the themes belonging to the specified training provider.
     *
     * By default, only a few basic details about each theme are returned,
     * but the optional ``fetch`` parameter may be used to fetch
     * additional attributes or references of each programme.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/provider/{shortName}/themes ]``
     *
     * @param string $shortName [required] The short name of the provider.
     * @param string $fetch [optional] A comma-separated list of any additional
     * details to fetch for each theme.
     *
     * @return UTBSTheme[] The provider's themes, in title order.
     */
    public function getThemes($shortName,
                              $fetch=null)
    {
        $pathParams = array("shortName" => $shortName);
        $queryParams = array("fetch" => $fetch);
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/provider/%1$s/themes',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->themes;
    }

    /**
     * Get all the venues belonging to the specified training provider.
     *
     * By default, only a few basic details about each venue are returned,
     * but the optional ``fetch`` parameter may be used to fetch
     * additional attributes or references of each programme.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/provider/{shortName}/venues ]``
     *
     * @param string $shortName [required] The short name of the provider.
     * @param string $fetch [optional] A comma-separated list of any additional
     * details to fetch for each venue.
     *
     * @return UTBSVenue[] The provider's venues, in name order.
     */
    public function getVenues($shortName,
                              $fetch=null)
    {
        $pathParams = array("shortName" => $shortName);
        $queryParams = array("fetch" => $fetch);
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/provider/%1$s/venues',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->venues;
    }
}
