<?php
/* === AUTO-GENERATED - DO NOT EDIT === */

/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once dirname(__FILE__) . "/../client/UTBSException.php";

/**
 * Methods for querying and manipulating bookings.
 *
 * **The fetch parameter for event bookings**
 *
 * All methods that return bookings also accept an optional
 * ``fetch`` parameter that may be used to request additional
 * information about the bookings returned. For more details about the
 * general rules that apply to the ``fetch`` parameter, refer to the
 * {@link EventMethods} documentation.
 *
 * For bookings the ``fetch`` parameter may be used to fetch details
 * about the individual sessions booked and the referenced event, people and
 * institutions:
 *
 * * ``"event"`` - fetches details about the event booked.
 *
 * * ``"participant"`` - fetches details about the person the booking
 *   is for.
 *
 * * ``"booked_by"`` - fetches details about the person who made the
 *   booking.
 *
 * * ``"institution"`` - fetches details about the participant's
 *   institution chosen at the time the booking was made.
 *
 * * ``"session_bookings"`` - fetches the session booking details.
 *
 * * ``"institutions"`` - fetches a list of all the participant's
 *   institutions, as they were at the time the booking was made.
 *
 * As with the event ``fetch`` parameter, this reference may be used
 * in a chain by using the "dot" notation to fetch additional information
 * about the referenced event or institutions. For example
 * "institution.school" will fetch the participant's chosen institution and
 * the school to which it belongs. For more information about what can be
 * fetched from the referenced event or institutions, refer to the
 * documentation for {@link EventMethods} and {@link InstitutionMethods}.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
class BookingMethods
{
    // The connection to the server
    private $conn;

    /**
     * Create a new BookingMethods object.
     *
     * @param ClientConnection $conn The ClientConnection object to use to
     * invoke methods on the server.
     */
    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    /**
     * Get the event booking with the specified ID.
     *
     * By default, only a few basic details about the booking are returned,
     * but the optional ``fetch`` parameter may be used to fetch
     * additional attributes or references.
     *
     * Note that viewing bookings requires authentication, and a booking is
     * only visible to the following:
     *
     * * The participant.
     *
     * * The person who made the booking (not necessarily the
     *   participant).
     *
     * * The event's trainers.
     *
     * * The event's administrator, owner and booker.
     *
     * * People with the "view-event-bookings" privilege for the event's
     *   provider.
     *
     * * People with the "view-3rd-party-booking" privilege for the event's
     *   provider.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/booking/{id} ]``
     *
     * @param int $id [required] The ID of the booking to fetch.
     * @param string $fetch [optional] A comma-separated list of any additional
     * details to fetch.
     *
     * @return UTBSEventBooking The requested booking or null if it was not found.
     */
    public function getBooking($id,
                               $fetch=null)
    {
        $pathParams = array("id" => $id);
        $queryParams = array("fetch" => $fetch);
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/booking/%1$s',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->booking;
    }

    /**
     * Update an event booking to record whether or not the participant
     * attended the event.
     *
     * The attendance may be updated for a single session on the event, by
     * specifying a particular session number, or for all booked sessions, by
     * passing ``null`` as the session number.
     *
     * The attendance may be set to ``true`` or ``false`` to indicate
     * whether or not the participant attended, or it may be set to
     * ``null`` (its original value) to indicate that their attendance
     * has not yet been recorded, or is currently unknown.
     *
     * Note that updating attendance requires authentication as a user with
     * permission to view the booking (see
     * {@link getBooking()}) and record attendance
     * on the event. So in addition to being able to view the booking, the
     * user must be either one of the following:
     *
     * * The event administrator.
     *
     * * A person with the "record-attendance" privilege for the event's
     *   provider.
     *
     * `` ``
     *
     * ``[ HTTP: PUT /api/v1/booking/{id}/attendance ]``
     *
     * @param int $id [required] The ID of the booking to update.
     * @param int $sessionNumber [optional] The session to update the attendance
     * for, or ``null`` to update all booked sessions (the default).
     * @param boolean $attended [optional] Whether or not the participant attended, or
     * ``null`` to record that their attendance has not been set (the
     * default).
     *
     * @return UTBSEventBooking The updated booking, together with all associated session
     * bookings.
     */
    public function updateAttendance($id,
                                     $sessionNumber=null,
                                     $attended=null)
    {
        $pathParams = array("id" => $id);
        $queryParams = array();
        $formParams = array("sessionNumber" => $sessionNumber,
                            "attended"      => $attended);
        $result = $this->conn->invokeMethod("PUT",
                                            'api/v1/booking/%1$s/attendance',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->booking;
    }

    /**
     * Update an event booking to record whether or not the participant
     * attended the event, and whether they passed or failed.
     *
     * The attendance and outcome may be updated for a single session on the
     * event, by specifying a particular session number, or for all booked
     * sessions, by passing ``null`` as the session number.
     *
     * The attendance and outcome may each be set to ``true`` or
     * ``false`` to indicate whether or not the participant attended or
     * passed, or it may be set to ``null`` (its original value) to
     * indicate that their attendance/outcome has not yet been recorded, or
     * is currently unknown.
     *
     * Note that updating attendance/outcome requires authentication as a user
     * with permission to view the booking (see
     * {@link getBooking()}) and record attendance
     * on the event. So in addition to being able to view the booking, the
     * user must be either one of the following:
     *
     * * The event administrator.
     *
     * * A person with the "record-attendance" privilege for the event's
     *   provider.
     *
     * `` ``
     *
     * ``[ HTTP: PUT /api/v1/booking/{id}/outcome ]``
     *
     * @param int $id [required] The ID of the booking to update.
     * @param int $sessionNumber [optional] The session to update the attendance
     * for, or ``null`` to update all booked sessions (the default).
     * @param boolean $attended [optional] Whether or not the participant attended, or
     * ``null`` to record that their attendance has not been set (the
     * default).
     * @param boolean $passed [optional] Whether or not the participant passed, or
     * ``null`` to record that their outcome has not been set (the
     * default).
     *
     * @return UTBSEventBooking The updated booking, together with all associated session
     * bookings.
     */
    public function updateOutcome($id,
                                  $sessionNumber=null,
                                  $attended=null,
                                  $passed=null)
    {
        $pathParams = array("id" => $id);
        $queryParams = array();
        $formParams = array("sessionNumber" => $sessionNumber,
                            "attended"      => $attended,
                            "passed"        => $passed);
        $result = $this->conn->invokeMethod("PUT",
                                            'api/v1/booking/%1$s/outcome',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->booking;
    }
}
