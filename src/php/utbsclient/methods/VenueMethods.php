<?php
/* === AUTO-GENERATED - DO NOT EDIT === */

/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once dirname(__FILE__) . "/../client/UTBSException.php";

/**
 * Methods for querying and manipulating venues.
 *
 * **The fetch parameter for venues**
 *
 * All methods that return venues also accept an optional ``fetch``
 * parameter that may be used to request additional information about the
 * venues returned. For more details about the general rules that apply to
 * the ``fetch`` parameter, refer to the {@link EventMethods}
 * documentation.
 *
 * For venues the ``fetch`` parameter may be used to fetch the
 * venue's provider:
 *
 * * ``"provider"`` - fetches the venue's provider.
 *
 * As with the event ``fetch`` parameter, this reference may be used
 * in a chain by using the "dot" notation to fetch additional information
 * about referenced provider. For example "provider.venues" will fetch the
 * venue's provider and all the other venues belonging to that provider. For
 * more information about what can be fetched from the referenced provider,
 * refer to the documentation for {@link ProviderMethods}.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
class VenueMethods
{
    // The connection to the server
    private $conn;

    /**
     * Create a new VenueMethods object.
     *
     * @param ClientConnection $conn The ClientConnection object to use to
     * invoke methods on the server.
     */
    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    /**
     * Return a list of all venues.
     *
     * By default, only a few basic details about each venue are returned,
     * but the optional ``fetch`` parameter may be used to fetch
     * additional attributes or references.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/venue/all-venues ]``
     *
     * @param string $fetch [optional] A comma-separated list of any additional
     * details to fetch for each venue.
     *
     * @return UTBSVenue[] The requested venues, in name order.
     */
    public function getAllVenues($fetch=null)
    {
        $pathParams = array();
        $queryParams = array("fetch" => $fetch);
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/venue/all-venues',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->venues;
    }

    /**
     * Get the venue with the specified ID.
     *
     * By default, only a few basic details about the venue are returned, but
     * the optional ``fetch`` parameter may be used to fetch
     * additional attributes or references.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/venue/{id} ]``
     *
     * @param int $id [required] The ID of the venue to fetch.
     * @param string $fetch [optional] A comma-separated list of any additional
     * details to fetch.
     *
     * @return UTBSVenue The requested venue or null if it was not found.
     */
    public function getVenue($id,
                             $fetch=null)
    {
        $pathParams = array("id" => $id);
        $queryParams = array("fetch" => $fetch);
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/venue/%1$s',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->venue;
    }

    /**
     * Get the events held in a venue in the specified time period.
     *
     * This will return any events that overlap the specified time period.
     * More specifically, it will return events whose start is less than or
     * equal to the end of the time period, and whose end is greater than or
     * equal to the start of the time period (i.e., all the start and end
     * timestamps are treated inclusively).
     *
     * Optionally, this will also check the event's sessions and exclude any
     * events that have no sessions overlapping the specified time period.
     * This can happen for events with multiple sessions. For example,
     * suppose an event has 2 sessions, one on Monday and the other on
     * Friday, and that the specified time period to search was on Wednesday.
     * Then by default, the event would be returned, because it starts on
     * Monday and ends on Friday, which overlaps the time period being
     * searched, but if session checking is enabled, the event would be
     * excluded.
     *
     * By default, only a few basic details about each event are returned,
     * but the optional ``fetch`` parameter may be used to fetch
     * additional attributes or references.
     *
     * NOTE: When using this API directly via the URL endpoints, date-time
     * parameters should be supplied as either milliseconds since epoch, or
     * as ISO 8601 formatted date or date-time strings.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/venue/{id}/events-in-time-period ]``
     *
     * @param int $id [required] The ID of the venue.
     * @param DateTime $start [optional] The start of the time period to search. If
     * omitted, this will default to 0:00am today.
     * @param DateTime $end [optional] The end of the time period to search. If
     * omitted, this will default to the first midnight after the start date.
     * @param boolean $checkSessions [optional] If ``true``, check the event
     * sessions, and exclude any events that have no sessions overlapping the
     * the time period being searched.
     * @param string $fetch [optional] A comma-separated list of any additional
     * details to fetch for each event.
     *
     * @return UTBSEvent[] A list of events found, in (start date-time, ID) order.
     */
    public function getEventsInTimePeriod($id,
                                          $start=null,
                                          $end=null,
                                          $checkSessions=null,
                                          $fetch=null)
    {
        $pathParams = array("id" => $id);
        $queryParams = array("start"         => $start,
                             "end"           => $end,
                             "checkSessions" => $checkSessions,
                             "fetch"         => $fetch);
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/venue/%1$s/events-in-time-period',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->events;
    }
}
