<?php
/* === AUTO-GENERATED - DO NOT EDIT === */

/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once dirname(__FILE__) . "/../client/UTBSException.php";

/**
 * Methods for querying and manipulating courses.
 *
 * Note that all the course details are actually on the individual events for
 * the course, and may vary each time the course is run, so most of the
 * useful methods for querying course details are actually in
 * {@link EventMethods}.
 *
 * **The fetch parameter for courses**
 *
 * All methods that return courses also accept an optional ``fetch``
 * parameter that may be used to request additional information about the
 * courses returned. For more details about the general rules that apply to
 * the ``fetch`` parameter, refer to the {@link EventMethods}
 * documentation.
 *
 * For courses the ``fetch`` parameter may be used to fetch
 * referenced events and themes. The following references are
 * supported:
 *
 * * ``"events"`` - fetches all the course's events, in (date, ID)
 *   order.
 *
 * * ``"themes"`` - fetches all the themes containing the course.
 *
 * As with the event ``fetch`` parameter, the references may be used
 * in a chain by using the "dot" notation to fetch additional information
 * about referenced events and themes. For example "events.sessions" will
 * fetch all the course's events and all their sessions. For more information
 * about what can be fetched from referenced events and themes, refer to the
 * documentation for {@link EventMethods} and {@link ThemeMethods}.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
class CourseMethods
{
    // The connection to the server
    private $conn;

    /**
     * Create a new CourseMethods object.
     *
     * @param ClientConnection $conn The ClientConnection object to use to
     * invoke methods on the server.
     */
    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    /**
     * Get the course with the specified ID.
     *
     * Note that a course by itself has very few attributes, since all the
     * interesting details are held on the events in the course, so typically
     * the optional ``fetch`` parameter should be used to fetch the
     * course's events.
     *
     * The ``fetch`` parameter may also be used to fetch the themes
     * that contain the course.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/course/{id} ]``
     *
     * @param string $id [required] The ID of the course to fetch.
     * @param string $fetch [optional] A comma-separated list of any additional
     * details to fetch.
     *
     * @return UTBSCourse The requested course or null if it was not found.
     */
    public function getCourse($id,
                              $fetch=null)
    {
        $pathParams = array("id" => $id);
        $queryParams = array("fetch" => $fetch);
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/course/%1$s',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->course;
    }

    /**
     * Get the events for the specified course in the specified time period.
     *
     * This will return any events that overlap the specified time period.
     * More specifically, it will return events whose start is less than or
     * equal to the end of the time period, and whose end is greater than or
     * equal to the start of the time period (i.e., all the start and end
     * timestamps are treated inclusively).
     *
     * Optionally, this will also check the event's sessions and exclude any
     * events that have no sessions overlapping the specified time period.
     * This can happen for events with multiple sessions. For example,
     * suppose an event has 2 sessions, one on Monday and the other on
     * Friday, and that the specified time period to search was on Wednesday.
     * Then by default, the event would be returned, because it starts on
     * Monday and ends on Friday, which overlaps the time period being
     * searched, but if session checking is enabled, the event would be
     * excluded.
     *
     * By default, only a few basic details about each event are returned,
     * but the optional ``fetch`` parameter may be used to fetch
     * additional attributes or references.
     *
     * NOTE: When using this API directly via the URL endpoints, date-time
     * parameters should be supplied as either milliseconds since epoch, or
     * as ISO 8601 formatted date or date-time strings.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/course/{id}/events-in-time-period ]``
     *
     * @param string $id [required] The ID of the course.
     * @param DateTime $start [optional] The start of the time period to search. If
     * omitted, this will default to 0:00am today.
     * @param DateTime $end [optional] The end of the time period to search. If
     * omitted, this will default to the first midnight after the start date.
     * @param boolean $checkSessions [optional] If ``true``, check the event
     * sessions, and exclude any events that have no sessions overlapping the
     * the time period being searched.
     * @param string $fetch [optional] A comma-separated list of any additional
     * details to fetch for each event.
     *
     * @return UTBSEvent[] A list of events found, in (start date-time, ID) order.
     */
    public function getEventsInTimePeriod($id,
                                          $start=null,
                                          $end=null,
                                          $checkSessions=null,
                                          $fetch=null)
    {
        $pathParams = array("id" => $id);
        $queryParams = array("start"         => $start,
                             "end"           => $end,
                             "checkSessions" => $checkSessions,
                             "fetch"         => $fetch);
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/course/%1$s/events-in-time-period',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->events;
    }

    /**
     * Get a course's self-taught events.
     *
     * By default, only a few basic details about each event are returned,
     * but the optional ``fetch`` parameter may be used to fetch
     * additional attributes or references.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/course/{id}/self-taught-events ]``
     *
     * @param string $id [required] The ID of the course.
     * @param string $fetch [optional] A comma-separated list of any additional
     * details to fetch for each event.
     *
     * @return UTBSEvent[] A list of self-taught events, in (title, ID) order.
     */
    public function getSelfTaughtEvents($id,
                                        $fetch=null)
    {
        $pathParams = array("id" => $id);
        $queryParams = array("fetch" => $fetch);
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/course/%1$s/self-taught-events',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->events;
    }
}
