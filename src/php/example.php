#!/usr/bin/php
<?php

require_once "utbsclient/client/UTBSClientConnection.php";
require_once "utbsclient/methods/EventMethods.php";

$conn = UTBSClientConnection::createTestConnection();
$em = new EventMethods($conn);

$e = $em->getEvent(58900, "sessions,topics");

print("\n");
print("Event: ".$e->title."\n");
print("\n");
print("Description\n");
print("===========\n");
print($e->description."\n");
print("\n");
print("Topics covered\n");
print("==============\n");
print($e->topics[0]->topic."\n");
print("\n");
foreach ($e->sessions as $s)
{
    print("Session ".$s->sessionNumber.":\n");
    print("  ".$s->startTime->format('Y-m-d H:i:s e').
          " - ".$s->endTime->format('Y-m-d H:i:s e')."\n");
}

?>
