utbsclient.connection module
============================

Classes:

@module.toc@

.. automodule:: utbsclient.connection
    :members: createConnection, createLocalConnection, createTestConnection
