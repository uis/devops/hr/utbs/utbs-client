.. University Training Booking System web service API documentation

UTBS web service client API
===========================

Modules:

.. toctree::
   :maxdepth: 1

   utbsclient.connection
   utbsclient.dto
   utbsclient.methods

This document describes the University Training Booking System web
service client API, which is intended to simplify the task of using
the web service from Python client applications.

This client library provides code to automatically handle the
following tasks:

* Securely connect to the server over HTTPS, checking its certificates
  to ensure that it is really is the UTBS server.

* Optionally authenticate with the server using HTTP Basic
  Authentication.

* Construct correct URLs to methods on the server, encoding any method
  parameters that may form part of the path, or appear in the query
  string.

* Encode any form parameters that may be needed in the request body.

* Parse the XML returned by the server, producing one or more linked
  Python objects containing all the requested information in an easily
  accessible form.

All of the API methods are contained in `XxxMethods`
classes in the :any:`methods` module, for example
:any:`ProgrammeMethods`
which contains methods relating to or returning programmes, and
:any:`EventMethods`
which contains methods relating to or returning events. All
the `XxxMethods` classes are auto-generated, to keep
them in sync with the matching server methods.

Some typical code using this API might look something like this:

.. code-block:: python

    from utbsclient import *

    conn = createTestConnection()
    pm = ProgrammeMethods(conn)

    prog = pm.getProgramme(55267, "events")
    events = prog.events

    print prog.title
    print "---"

    for event in events:
        if event.startDate:
            print(event.startDate.isoformat()+": "+event.title)

Note that it is typically only necessary to create a single instance
of the :any:`UTBSClientConnection` and `XxxMethods`
objects, and then re-use them throughout your application.

**Thread safety**

Once created and initialised, the :any:`UTBSClientConnection` and
`XxxMethods` classes are safe to use from multiple
simultaneous threads (for example in a web server).

Note, however, that
:any:`UTBSClientConnection.set_username()` and
:any:`UTBSClientConnection.set_password()` are not thread safe. These methods
are regarded as part of the initialisation of the connection, and
should typically only be used once on startup. If you need to
regularly switch between users, then use a separate
:any:`UTBSClientConnection` per user or per thread (and separate
corresponding `XxxMethods` objects).
