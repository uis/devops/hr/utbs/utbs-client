UTBS web service API client
===========================

The utbs-client provides Java, Python and PHP client code for accessing the
UTBS web service API.

This client code simplifies the task of using the UTBS web service API by
providing code to handle the following:

* Securely connect to the UTBS web server using HTTPS.

* Check the server certificates.

* Optionally authenticate with the server using HTTP Basic Authentication.
  This is required for certain API methods that access non-public data, or
  that perform data updates.

* Construct correct URLs for all API methods on the server.

* Encode any method parameters that may form part of the URL.

* Encode and send any required form parameters for API methods that use HTTP
  PUT or POST methods.

* Parse the XML returned by the server, producing objects of the appropriate
  classes, with fields of the appropriate types.

See <https://www.training.cam.ac.uk/doc/ws-doc/>

[LICENCE](https://gitlab.developers.cam.ac.uk/uis/sysdev/devgroup/utbs/utbs-client/raw/HEAD/lgpl.txt)
