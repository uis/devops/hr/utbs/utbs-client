"""
UTBS web service API

Python installation.
"""

from setuptools import setup

setup(
    name="utbsclient",
    version="1.3.2",
    description="UTBS API client",
    license="LGPL",
    url="https://training.cam.ac.uk/doc/ws-doc/",
    packages=["utbsclient"],
    package_dir={"utbsclient": "src/python3/utbsclient"},
    install_requires=[
        "requests~=2.26",
    ]
)
